---
redirect_to: 'https://docs.gitlab.com/ee/administration/pages/index.html'
remove_date: '2021-01-22'
---

This document was moved to [another location](https://docs.gitlab.com/ee/administration/pages/index.html)
